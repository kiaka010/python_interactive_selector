class Player:
    id = None
    name = None
    image = None
    offset = (None, None)

    def set_id(self, id):
        self.id = id

    def get_id(self):
        return self.id

    def set_name(self, name):
        self.name = name

    def get_name(self):
        return self.name

    def set_image(self, image):
        self.image = image

    def get_image(self):
        return self.image

    def set_offset(self, x, y):
        self.offset = (x, y)

    def get_offset(self):
        return self.offset

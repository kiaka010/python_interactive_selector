from Player import Player

class Req:

    players = []
    headerImage = None
    sponsorImage = None
    backgroundImage = None
    title = None

    def set_header_image(self, headerImage):
        self.headerImage = headerImage

    def get_header_image(self):
        return self.headerImage

    def set_sponsor_image(self, sponsorImage):
        self.sponsorImage = sponsorImage

    def get_sponsor_image(self):
        return self.sponsorImage

    def set_background_image(self, backgroundImage):
        self.backgroundImage = backgroundImage

    def get_background_image(self):
        return self.backgroundImage

    def set_title_image(self, title):
        self.title = title

    def get_title_image(self):
        return self.title

    def set_players(self, players):
        self.players = players

    def get_players(self):
        return self.players
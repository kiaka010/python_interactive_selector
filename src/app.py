from flask import Flask, render_template, request, send_file
import shutil
import os
from generator import Hydrate, Generate
app = Flask(__name__)
import logging
from pprint import pprint
logger = logging.getLogger('image.run')

@app.route("/submit", methods=['POST'])
def main():

    content = request.get_json()
    hydrator = Hydrate()
    req = hydrator.hydrate(content)

    logger.info("Starting Generator")
    gen = Generate()
    a = gen.start(req)
    logger.info("Stopping Generator")
    return send_file('out.jpg', mimetype='image/jpg')

@app.route("/clean", methods=['POST'])
def clean():
    logger.info("Cleaning")
    # os.remove('out.jpg')
    shutil.rmtree('cache')
    os.makedirs('cache')
    logger.info("Clean Complete")
    return 'success'

if __name__ == "__main__":
    log_level = logging.DEBUG
    logging.basicConfig(format='%(asctime)s - %(name)s[%(process)d] - %(levelname)s - %(message)s')
    logger.setLevel(log_level)
    logging.getLogger('image.run').setLevel(log_level)
    app.run()
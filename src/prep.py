# Import the modules
from hydrate import convert
import json
import argparse
from pprint import pprint
import logging
logger = logging.getLogger('image.run')


class Prep:
    playerGroups = {}
    def start(self, selection, structure):
        categories = structure['categories']
        formations = structure['formations']
        if selection['formation'] not in formations:
            exit()
        formation = formations[selection['formation']]
        for participant in selection['participants']:
            for posNum, posName in participant['position']['filter'].iteritems():
                if posName not in self.playerGroups:
                    self.playerGroups[posName] = []
                self.playerGroups[posName].append([participant['id'], participant['longName']])
        # pprint(self.playerGroups)

        pprint(formation)










def main():

    # exit()
    # Setup argument parser
    parser = argparse.ArgumentParser(description='Gets unique skyq feed urls')


    # Process arguments
    log_level = logging.DEBUG
    args = parser.parse_args()

    logging.basicConfig(format='%(asctime)s - %(name)s[%(process)d] - %(levelname)s - %(message)s')
    logger.setLevel(log_level)
    logging.getLogger('image.run').setLevel(log_level)

    logger.info("Starting Prep")

    with open('example/create.json') as data_file:
        data = json.load(data_file)
    with open('example/formations.json') as data_file:
        formationdata = json.load(data_file)
    request =convert(data)
    formationdata = convert(formationdata)
    formationdata = formationdata['football']
    compare = Prep()
    compare.start(request, formationdata)
    logger.info("Stopping Prep")
    exit()

# This bit of code allows the parser to be imported and not have the main()
# execute
if __name__ == "__main__":
    try:
        main()
    except (KeyboardInterrupt, SystemExit):
        print "Exiting"
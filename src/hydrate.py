from Entity.Player import Player
from Entity.Request import Req
import collections

def convert(data):
    if isinstance(data, basestring):
        return str(data)
    elif isinstance(data, collections.Mapping):
        return dict(map(convert, data.iteritems()))
    elif isinstance(data, collections.Iterable):
        return type(data)(map(convert, data))
    else:
        return data

class Hydrate:

    def hydrate(self, json):
        json = convert(json)
        req = Req()
        req.set_background_image(json['backgroundImage'])
        req.set_sponsor_image(json['sponsorshipImage'])
        req.set_header_image(json['headerImage'])
        if('title' in json):
            req.set_title_image(json['title'])
        players = []
        for player in json['players']:
            # pprint(player)
            playerObj = Player()
            playerObj.set_id(player['id'])
            playerObj.set_name(player['name'])
            playerObj.set_image(player['image'])
            playerObj.set_offset(player['x'],player['y'])
            players.append(playerObj)
        req.set_players(players)
        return req

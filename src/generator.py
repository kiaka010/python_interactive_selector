# Import the modules
from PIL import Image, ImageFont, ImageDraw
from hydrate import Hydrate
import sys
import urllib, cStringIO
import os
import base64
import json
import argparse
import logging
import shutil
from multiprocessing import Pool, Process
from progress.bar import Bar
from pprint import pprint
from threaded_download import ThreadedDownload
import re
logger = logging.getLogger('image.run')

class Generate:
    background = None
    font = None

    def _prep(self, req):
        pool = Pool(processes=4)
        urls = []
        urls.append(req.get_sponsor_image())
        urls.append(req.get_background_image())
        urls.append(req.get_header_image())

        for player in req.get_players():
            urls.append(player.get_image())

        bar = Bar('Processing', max=len(urls))
        for i in pool.imap(job, urls):
            bar.next()
        bar.finish()

    def _get_image(self, path):
        return 'cache/' + str(path.split('/')[-1])

    def _draw_title(self, request ,(W, H)):
        msg = request.get_title_image()
        draw = ImageDraw.Draw(self.background)
        w, h = draw.textsize(msg, font=self.font)
        draw.text(((W-w)/2,10), msg, fill="white", font=self.font)


    def _draw_title_image(self, request ,(W, H)):
        headerImg = Image.open(self._get_image(request.get_header_image()), 'r')
        imgW, imgH = headerImg.size

        if(imgW > W):
            percentChange = (float(W) / float(imgW)) * 100

            headerImg = headerImg.resize((W,int(imgH / 100.0 * percentChange)), Image.ANTIALIAS)
            imgW, imgH = headerImg.size

        self.background.paste(headerImg, (((imgW - W)/2),10), headerImg)

    def start(self, request):
        self._prep(request)
        self.background = Image.open(self._get_image(request.get_background_image()))
        self.font = ImageFont.truetype("Sky_Reg.ttf", 32)

        W, H = self.background.size


        if(request.get_title_image()) :
            self._draw_title(request, self.background.size)
        else :
            self._draw_title_image(request, self.background.size)


        # players = []
        # for player in request.get_players():
        #     players.append([player.get_image(), player.get_offset(), player.get_name()])
        # p = Pool(2)
        # p.map(self._add_player, players)
        for player in request.get_players():
            self._add_player(player.get_image(), player.get_offset(), player.get_name())


        self.background.save('out.jpg')



    def _add_player(self,url, offset, name):
        x, y = offset
        # file = cStringIO.StringIO(urllib.urlopen(url).read())
        img = Image.open(self._get_image(url), 'r')
        self.background.paste(img, offset, img)

        imgW, imgH = img.size


        draw = ImageDraw.Draw(self.background)
        self.font = ImageFont.truetype("Sky_Reg.ttf", 25)
        msgW, msgH = draw.textsize(name, font=self.font)

        draw.text((x - ((msgW - imgW) / 2 ), y + imgH), name, fill="white", font=self.font)
def job(url):

    file_name = str(url.split('/')[-1])
    if os.path.exists('cache/' + file_name):
        logger.debug('File ' + file_name + ' has already been downloaded')
        return True
    u = urllib.urlopen(url)
    f = open('cache/' +file_name, 'wb')
    f.write(u.read())
    f.close()

def main():

    # exit()
    # Setup argument parser
    parser = argparse.ArgumentParser(description='Gets unique skyq feed urls')
    parser.add_argument('--clean', '-c', dest='clean',  action='store_true')


    # Process arguments
    log_level = logging.DEBUG
    args = parser.parse_args()
    clean = args.clean

    logging.basicConfig(format='%(asctime)s - %(name)s[%(process)d] - %(levelname)s - %(message)s')
    logger.setLevel(log_level)
    logging.getLogger('image.run').setLevel(log_level)

    if (clean):
        logger.info("Cleaning")
        # os.remove('out.jpg')
        shutil.rmtree('cache')
        os.makedirs('cache')
        logger.info("Clean Complete")
        exit()


    logger.info("Starting Generator")

    with open('example/data.json') as data_file:
        data = json.load(data_file)
    hydrator = Hydrate()
    request = hydrator.hydrate(data)

    compare = Generate()
    compare.start(request)
    logger.info("Stopping Generator")
    exit()


# This bit of code allows the parser to be imported and not have the main()
# execute
if __name__ == "__main__":
    try:
        main()
    except (KeyboardInterrupt, SystemExit):
        print "Exiting script"